import React, { useEffect, useState } from 'react'
import Header from '../../common/components/Header/header'
import { useDispatch, useSelector } from 'react-redux'
import { Container, Grid } from '@mui/material'
import FavoriteItem from './FavoriteItem/favoriteItem'
import { fetchingBlogsAction } from '../../redux/actions/homeAction'

const FavoritePage = () => {
  const [page, setPage] = useState(0)
  const dispatch = useDispatch()
  const ids = useSelector(state =>state.ids.ids)
  const articles = useSelector(state=>state.dataBlogsFetch.articles)
  const totalPages = useSelector(state=>state.dataBlogsFetch.totalPages)
  if(totalPages <= page){
      setPage(totalPages-1)
  }
  useEffect(() => {
      dispatch(fetchingBlogsAction(page,1000))
  },[page])

  const favoriteArticle = () =>{
    if(!articles){
        return []
    }else{
      return articles.filter(article => ids.includes(article.id))
      }  
  }
  return (
    <>
      <Header/>
      
        <Container>
          <h1>Bài viết yêu thích</h1>
          <div className='blog-list'>
            
                    <Grid container rowSpacing={1} columnSpacing={{ xs: 1, sm: 2, md: 3 }}>
                        {favoriteArticle().map((article)=>{
                            return(
                                            <FavoriteItem
                                                key={article.id}
                                                article={article}
                                            />
                            )
                        })}
                        
                    </Grid>
          </div>
          </Container>
      
    </>
  )
}

export default FavoritePage
