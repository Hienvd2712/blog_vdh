import { Button } from 'react-bootstrap'
import './cmtBlog.css'
import { useState } from 'react'
import { useNavigate, useParams } from 'react-router-dom'
import cmtService from '../../../../common/api/cmtService'
import { useDispatch, useSelector } from 'react-redux'
import { useEffect } from 'react'
import { fetchingPostsCmtAction, sendingPostsCmtAction } from '../../../../redux/actions/cmtAction'
const CmtBlogs = () =>{
    const [comment, setComment] = useState('')
    const dispatch = useDispatch()
    const params = useParams()
    const comments = useSelector(state=>state.commentsPosts.comments)
    useEffect(()=>{
        dispatch(fetchingPostsCmtAction(0,50,params.id))
    },[])
    const handleCmtInput = (e) =>{
        setComment(e.target.value)
    }
    
    const handleCmtClicked = (e) =>{
        e.preventDefault()
        setComment('')
        dispatch(sendingPostsCmtAction(params.id, comment))
    }
    return(
        <>
        <h1>Comments</h1>
        <div className='cmt-container'>
            {comments.map((cmt)=>{
            return(
                <div key={cmt.id}>
            <div>
                <span >
                    <img src={cmt.commentBy.avatarUrl}
                        className="rounded-circle avatar-url"
                        onError={(e)=>{e.target.onerror=null; e.target.src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ4LlAPpfponKu-b1wHsaI7jpBNny56AuOZyg&usqp=CAU"}}/>
                </span>
                <span className='cmt-name'>
                    {cmt.commentBy.fullName}
                </span>
            </div>
            <div className='content'>{cmt.content}</div>
            </div>
            )})}
            
            
        </div>
        
        <input type='text' 
        className="form-control cmt-add" 
        placeholder="Nhập vào bình luận của bạn" 
        onChange={handleCmtInput}
        value={comment}
        />
        <Button onClick={handleCmtClicked}>Bình luận</Button>
        
        </>
    )
}
export default CmtBlogs