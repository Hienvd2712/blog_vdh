import React, { useEffect, useState } from 'react';
import { Avatar, Container, Grid } from '@mui/material';
import './blogPost.css'
import useResize from '../../../hooks/useResize'
import FacebookIcon from '@mui/icons-material/Facebook';
import InstagramIcon from '@mui/icons-material/Instagram';
import TwitterIcon from '@mui/icons-material/Twitter';
import YouTubeIcon from '@mui/icons-material/YouTube';
import { useParams } from 'react-router-dom'
import CmtBlogs from './cmtBlog/cmtBlog'
import Heart from '../../../common/components/Heart/heart'
import { useDispatch, useSelector } from 'react-redux'
import { addFavoriteAction, removeFavoriteAction } from './redux/action'
import GoToTopButton from './GoToTopButton/goToTopButton'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faBookOpen, faCalendarDays } from '@fortawesome/free-solid-svg-icons'
import { fetchingPostsAction } from '../../../redux/actions/postAction'

const BlogPost = () => {
  
  const size = useResize()
  const params = useParams()
  const dispatch = useDispatch()

  const ids = useSelector(state => state.ids.ids)
  const avatarUrl = useSelector(state =>state.dataPostsFetch.avatarUrl)
  const fullName = useSelector(state =>state.dataPostsFetch.fullName)
  const coverImageUrl = useSelector(state =>state.dataPostsFetch.coverImageUrl)
  const title = useSelector(state =>state.dataPostsFetch.title)
  const estDuration = useSelector(state =>state.dataPostsFetch.estDuration)
  const content = useSelector(state =>state.dataPostsFetch.content)
  const createdDate = useSelector(state =>state.dataPostsFetch.createdDate)

  const [favorite, setFavorite] = useState(()=>{
            return ids.includes(params.id)
        })

  useEffect(()=>{
    dispatch(fetchingPostsAction(params.id))
  },[])

  const handleFavoriteButtonClicked = ()=>{
  if(!favorite){
      dispatch(addFavoriteAction(params.id))
    }
    else{
      dispatch(removeFavoriteAction(params.id))
    }
    setFavorite(!favorite)
  }

  return (
    <Container>
      <Grid container spacing={2} style={{marginTop: 30}}>
        <Grid item xs={12} md={9}  >
          <div className='blog-post'>
            <div className='post-img'>
              {coverImageUrl!=null ? (<img 
              src={coverImageUrl}
              onError={(e)=>{e.target.onerror=null; e.target.src="https://media.istockphoto.com/id/931643150/vector/picture-icon.jpg?s=170667a&w=0&k=20&c=3Jh8trvArKiGdBCGPfe6Y0sUMsfh2PrKA0uHOK4_0IM="}}
              />):(
                <img src='https://media.istockphoto.com/id/931643150/vector/picture-icon.jpg?s=170667a&w=0&k=20&c=3Jh8trvArKiGdBCGPfe6Y0sUMsfh2PrKA0uHOK4_0IM='/>
              )}
            </div>
            <h1 className='post-title'>{title}</h1>
            {size.width>=768 ?(
              <div className='post-info-pc'>
                <div className='post-author'>
                  <Avatar src={avatarUrl} />
                  <div id='author-name'>{fullName}</div>
                </div>
                <div className='post-date-duration'><FontAwesomeIcon icon={faCalendarDays} /> {createdDate}</div>
                <div className='post-date-duration'><FontAwesomeIcon icon={faBookOpen} /> {estDuration} phút đọc</div>
              </div>
            ):(
              <div className='post-info-mb'>
             <div className='post-author'>
                  <Avatar src={avatarUrl} />
                  <div id='author-name'>{fullName}</div>
                </div>
                <div className='post-date-duration'><FontAwesomeIcon icon={faCalendarDays} /> {createdDate}</div>
                <div className='post-date-duration'><FontAwesomeIcon icon={faBookOpen} /> {estDuration} phút đọc</div>
            </div>
            )}
          </div>
         <p dangerouslySetInnerHTML={{__html:content}}></p>
         <div className="post-favorite-icon"><Heart isFavorite={favorite} onClick={handleFavoriteButtonClicked} /></div>
                    <div className="post-favorite">Yêu thích</div>
         <CmtBlogs 
            
          />
        </Grid>
        <Grid item sx={{display:{xs:'none', md:'inline'}}} md={3}  >
          <div>
            <div className='post-add'>About me</div>
            <div className='avatar-add' > 
              <img src={avatarUrl} className="rounded-circle"/>
            </div>   
            <h1 className='authorName-add'>{fullName}</h1>
            <div className='post-add'>Follow me</div>
            <div className='icon-follow'>
              <div className='social-follow'><FacebookIcon style={{color:"#0d6efd", width:"30px", height:"30px"}}/></div>
              <div className='social-follow' ><InstagramIcon style={{color:"#d6249f", width:"30px", height:"30px"}}/></div>
              <div className='social-follow' ><TwitterIcon style={{color:"wheat", width:"30px", height:"30px"}}/></div>
              <div className='social-follow' ><YouTubeIcon style={{color: "red", width:"30px", height:"30px"}}/></div>
            </div>

            
          </div>
        </Grid>
  
      </Grid>
      <GoToTopButton/>
    </Container>
  )
}

export default BlogPost
