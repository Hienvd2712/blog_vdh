import React, { useEffect, useState } from "react";
import { Bar } from "react-chartjs-2";
import { Chart as ChartJS,
        BarElement,
        CategoryScale,
        LinearScale,
        Tooltip,
        Legend
} from "chart.js";
import './statisticPage.css'
import { useDispatch, useSelector } from "react-redux";
import { fetchingStatisticAction } from "../../../redux/actions/statisticAction";


ChartJS.register(
    BarElement,
        CategoryScale,
        LinearScale,
        Tooltip,
        Legend
)
const BlogStatistical = () =>{
    const dispatch = useDispatch()
    const data = useSelector(state => state.statistical.data)
    const labels = useSelector(state => state.statistical.labels)
    useEffect(()=>{
       dispatch(fetchingStatisticAction)
    },[])
    const dataStatistic = {
        labels: labels,
        datasets: [
            {
                label: 'Số lượt xem',
                data:data,
                backgroundColor: 'skyblue',
                borderColor: 'black',
                borderWidth: 1,
            }
        ]
    }
    const options = {}
    return(
        <div>
            <h1>Thống kê</h1>
            <div className="statistical-container">
                <div className="statistical-bar">
                    <Bar 
                        data={dataStatistic}
                        options={options}  
                    />
                </div>
            </div>
        </div>
    )
}

export default BlogStatistical