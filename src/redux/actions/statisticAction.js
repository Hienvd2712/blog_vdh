import BlogAdminService from '../../common/api/blogAdminService'
import { FETCH_STATISTIC_ADMIN_REQUEST } from '../constants/constant'



export const fetchedStatisticAction =(data, labels)=>{
    return {
        type: FETCH_STATISTIC_ADMIN_REQUEST,
        payload: {data, labels}
    }
}

export const fetchingStatisticAction = (dispatch) =>{
    BlogAdminService.getStatistic()
    .then(response=>{
        const data = response.data.data
        const labels = response.data.labels
        dispatch(fetchedStatisticAction(data, labels))
    })
    .catch(error =>{
        console.log(error);
    })

}