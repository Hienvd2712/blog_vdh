import blogService from "../../common/api/blogService"
import { FETCH_POSTS_REQUEST } from "../constants/constant"

export const fetchedPostsAction =(dataResponse)=>{
    return {
        type: FETCH_POSTS_REQUEST,
        payload: dataResponse
    }
}

export const fetchingPostsAction = (articleId) =>{
    return dispatch =>{
    blogService.getArticleId(articleId)
    .then(response=>{
        const articles = response.data
        const author = articles.author
            const avatarUrl = author.avatarUrl
            const fullName = author.fullName
        const coverImageUrl = articles.coverImageUrl
        const title = articles.title
        const estDuration = articles.estDuration
        const content = articles.content
        const createdDate = articles.createdDate
        const dataResponse = {avatarUrl, fullName, coverImageUrl, title, estDuration, content, createdDate }
        dispatch(fetchedPostsAction(dataResponse))
    })
    .catch(error =>{
        console.log(error);
    })
}
}