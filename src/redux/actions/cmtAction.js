import cmtService from "../../common/api/cmtService"
import { FETCH_POSTS_CMT_REQUEST, SEND_POSTS_CMT_REQUEST } from "../constants/constant"



export const fetchedPostsCmtAction =(comments)=>{
    return {
        type: FETCH_POSTS_CMT_REQUEST,
        payload: comments
    }
}
export const fetchingPostsCmtAction = (page, size, articleId) =>{
    return (dispatch) =>{
    cmtService.getComment(page, size, articleId)
    .then(response=>{
        const comments = response.data.content
         
        dispatch(fetchedPostsCmtAction(comments))
    })
    .catch(error =>{
        console.log(error);
    })
}
}

export const sendingPostsCmtAction = (articleId, content) =>{
    
    return (dispatch) =>{
    cmtService.postComment(articleId, content)
    .then(response=>{ 
        const comment = response.data       
        dispatch({
            type: SEND_POSTS_CMT_REQUEST,
            payload: comment
        })  
    })
    .catch(error =>{    
        console.log(error);
       alert('Vui lòng đăng nhập để bình luận')
    })
}
}


