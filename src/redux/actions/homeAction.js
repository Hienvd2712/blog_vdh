
import {
    FETCH_ARTICLES_REQUEST,
   
} from '../constants/constant'
import blogService from '../../common/api/blogService'


export const fetchedBlogsAction =(articles, totalPages)=>{
    return {
        type: FETCH_ARTICLES_REQUEST,
        payload: {articles, totalPages}
    }
}

export const fetchingBlogsAction = (page,size) =>{
    return dispatch =>{
    blogService.getArticle(page,size)
    .then(response=>{
        const articles = response.data.content
        const totalPages = response.data.totalPages
        dispatch(fetchedBlogsAction(articles, totalPages))
    })
    .catch(error =>{
        console.log(error);
    })
}
}
