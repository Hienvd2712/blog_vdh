import { DELETE_ARTICLES_ADMIN_REQUEST, FETCH_ARTICLES_ADMIN_REQUEST } from '../constants/constant'
import BlogAdminService from '../../common/api/blogAdminService'
import { useState } from 'react'

// GET ARTICLES ACTION
export const fetchedArticlesAdminAction =(articles, totalPages)=>{
    return {
        type: FETCH_ARTICLES_ADMIN_REQUEST,
        payload: {articles, totalPages}
    }
}
export const fetchingArticlesAdminAction = (page,size,body) =>{
    return dispatch =>{
    BlogAdminService.getArticle(page,size,body)
    .then(response=>{
        const articles = response.data.content
        const totalPages = response.data.totalPages
        dispatch(fetchedArticlesAdminAction(articles, totalPages))
    })
    .catch(error =>{
        console.log(error);
    })
}
}

// DELETE
export const deleteArticlesAdminAction = (articleId,page,size,body) =>{
    return dispatch =>{
    BlogAdminService.deleteArticle(articleId)
    .then(response=>{
        alert('Xóa bài viết thành công');
        dispatch({
            type: DELETE_ARTICLES_ADMIN_REQUEST,   
        })
        dispatch(fetchingArticlesAdminAction(page,size,body))
        
    })
    .catch(error =>{
        alert('Đã xảy ra lỗi vui lòng thử lại')
    })
}
}
// CREATE

