import { applyMiddleware, combineReducers, createStore } from "redux";

import thunk from "redux-thunk";
import favoriteReducer from "../pages/HomePage/BlogPost/redux/favoriteReducer";
import homeReducer from "./reducers/homeReducer";
import postReducer from "./reducers/postReducer";
import cmtReducer from "./reducers/cmtReducer";
import statisticReducer from "./reducers/statisticReducer";
import adminReducer from "./reducers/adminReducer";
const store = createStore(combineReducers({
    ids: favoriteReducer,
    dataBlogsFetch: homeReducer,
    dataPostsFetch: postReducer,
    commentsPosts: cmtReducer,
    statistical: statisticReducer,
    adminCrud: adminReducer
}), applyMiddleware(thunk)) 

export default store;