import {
    FETCH_POSTS_CMT_REQUEST,
    SEND_POSTS_CMT_REQUEST,  
} from '../constants/constant'

const initialState = {
    comments: [],
   
    
}

const cmtReducer = (state = initialState, action) =>{
    switch(action.type){
        case FETCH_POSTS_CMT_REQUEST:
            return {...state,
                    comments: action.payload,
                    }
        case SEND_POSTS_CMT_REQUEST:
            return {...state,
                    comments: [...state.comments, action.payload]}    
                    
                    
        default:
            return state
    }
}

export default cmtReducer