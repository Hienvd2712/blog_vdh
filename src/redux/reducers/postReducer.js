import {
    FETCH_POSTS_REQUEST,
    
    
} from '../constants/constant'

const initialState = {
    avatarUrl:'',
    fullName:'',
    coverImageUrl:'',
    title:'',
    estDuration:'',
    content:'',
    createdDate:''
    
}

const postReducer = (state = initialState, action) =>{
    switch(action.type){
        case FETCH_POSTS_REQUEST:
            return {...state,
                        avatarUrl: action.payload.avatarUrl,
                        fullName: action.payload.fullName,
                        coverImageUrl: action.payload.coverImageUrl,
                        title: action.payload.title,
                        estDuration: action.payload.estDuration,
                        content: action.payload.content,
                        createdDate: action.payload.createdDate
                    }
        default:
            return state
    }
}

export default postReducer