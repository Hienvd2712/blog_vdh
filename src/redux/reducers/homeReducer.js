import {
    FETCH_ARTICLES_REQUEST,
    
} from '../constants/constant'

const initialState = {
    articles: [],
    
}

const homeReducer = (state = initialState, action) =>{
    switch(action.type){
        case FETCH_ARTICLES_REQUEST:
            return {...state,
                    articles: action.payload.articles,
                    totalPages: action.payload.totalPages
                    }
        default:
            return state
    }
}

export default homeReducer