import { DELETE_ARTICLES_ADMIN_REQUEST, FETCH_ARTICLES_ADMIN_REQUEST } from '../constants/constant'

const initialState = {
    articles: [],
   
}

const adminReducer = (state = initialState, action) =>{
   
    
    switch(action.type){
        case FETCH_ARTICLES_ADMIN_REQUEST:
            return {...state,
                    articles: action.payload.articles,
                    totalPages: action.payload.totalPages,
                    
                    }
        case DELETE_ARTICLES_ADMIN_REQUEST:
            return {...state}         
        default:
            return state
    }
}

export default adminReducer