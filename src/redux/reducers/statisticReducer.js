import { FETCH_STATISTIC_ADMIN_REQUEST } from '../constants/constant'

const initialState = {
    data:[],
    labels:[],
   
    
}

const statisticReducer = (state = initialState, action) =>{
    switch(action.type){
        case FETCH_STATISTIC_ADMIN_REQUEST:
            return {...state,
                        data: action.payload.data,
                        labels: action.payload.labels,
                    }
        default:
            return state
    }
}

export default statisticReducer